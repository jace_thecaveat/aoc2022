
if __name__ == '__main__':
    file = open("../input.txt")
    num_list = list(file)
    elf = []
    calorie_list = []
    for num in num_list:
        if num != "\n":
            elf.append(int(num.replace("\n", "")))
        else:
            calorie_list.append(elf[:])
            elf.clear()
            continue

    calorie_totals = []

    for cals in calorie_list:
        calorie_totals.append(sum(cals))
    calorie_totals.sort()
    calorie_totals.reverse()
    print(sum(calorie_totals[0:3]))