package com.aoc.part2;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.util.*;
import java.util.stream.Collectors;

public class Main {

    public static int priority(List<String> rucksacks){
        int priority = 0;
        Character triplicatePouchElement = ' ';
        HashMap<Character, Integer> matches = new HashMap<>();

        String first = rucksacks.get(0);
        String second = rucksacks.get(1);
        String third = rucksacks.get(2);

        Set<Character> firstSet = new TreeSet<Character>();
        Set<Character> secondSet = new TreeSet<Character>();
        Set<Character> thirdSet = new TreeSet<>();

        firstSet.addAll(first.chars().mapToObj(c -> (char) c).collect(Collectors.toList()));
        secondSet.addAll(second.chars().mapToObj(c -> (char) c).collect(Collectors.toList()));
        thirdSet.addAll(third.chars().mapToObj(c -> (char) c).collect(Collectors.toList()));

        for(Character c : firstSet){
                matches.put(c, 1);
        }

        for(Character c : secondSet){
            if(matches.containsKey(c)){
                matches.put(c, matches.get(c) + 1);
            }else{
                matches.put(c, 1);
            }
        }

        for(Character c : thirdSet){
            if(matches.containsKey(c)){
                matches.put(c, matches.get(c) + 1);
            }else{
                matches.put(c, 1);
            }
        }

        Map<Character, Integer> filteredSack = matches.entrySet()
                .stream().filter(x -> x.getValue() == 3)
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));

        triplicatePouchElement = (Character) filteredSack.keySet().toArray()[0];

        if(Character.isUpperCase(triplicatePouchElement)){
            priority = (int) triplicatePouchElement - 64 + 26;
        }else{
            priority = (int) triplicatePouchElement - 96;
        }

        return priority;
    }

    public static void main(String[] args) {
        BufferedReader reader;
        List<String> rucksacks = new ArrayList<>();
        List<List<String>> elfGroups = new ArrayList<>();
        int sum = 0;

        try{
            reader = new BufferedReader(new FileReader("./input.txt"));
            String line = reader.readLine();
            while (line != null){
                //List compartments = List.of(line.substring(0, line.length()/2), line.substring(line.length()/2));
                rucksacks.add(line);
                line = reader.readLine();
            }
        }catch (IOException e) {
            e.printStackTrace();
        }

        for(int i = 0; i < rucksacks.size(); i += 3){
            elfGroups.add(rucksacks.subList(i, i + 3));
        }

        for(List l: elfGroups){
            sum += priority(l);
        }
        
        System.out.println(sum);
    }
}
