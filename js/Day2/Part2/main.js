#!/usr/bin/env node

const f = require('fs');
const readline = require('readline');
let strategy = [];
var user_file = '../input.txt';
const shapeValue = [1, 2, 3];

function winOrLose(opp, end){

    let total = 0;

    if(end == 0) { // The end is a loss
        if(opp == 2){ //Scissors
            total += shapeValue[opp - 1];
        }else if(opp == 0){ //Rocks
            total += shapeValue[opp + 2];
        }else{ //Paper
            total += shapeValue[opp - 1];
        }
    }else{ // the end is a win `end == 2`
        if(opp == 2){ //Scissors
            total += 6 + shapeValue[opp -2];
        }else if(opp == 0){ //Rocks
            total += 6 + shapeValue[opp + 1];
        }else{ //Paper
            total += 6 + shapeValue[opp + 1];
        }
    }
    return total;
}

function totalScore(scoreText){
    let scoreArray = [scoreText[0].charCodeAt() % 65, (scoreText[1].charCodeAt() % 65) - 23];
    let oppChoice = scoreArray[0];
    let endRound = scoreArray[1];
    let completeScore = 0;
    if(endRound === 1){
        completeScore += shapeValue[oppChoice] + 3;
    }else{
        completeScore += winOrLose(oppChoice, endRound);
    }
    return completeScore;
}

function scoreRound(round){
    let shapes = round.split(" ");
    let finalScore = totalScore(shapes);
    return finalScore;
}

async function processStrategy(){
    var r = readline.createInterface({
        input : f.createReadStream(user_file)
    });
    for await (const line of r) {
        strategy.push(line);
    }
    let roundSum = 0;
    for(const roundScore of strategy){
        roundSum += scoreRound(roundScore);
    }
    console.log(roundSum);
}

processStrategy();