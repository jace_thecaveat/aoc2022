#!/usr/bin/env node

const f = require('fs');
const readline = require('readline');
let strategy = [];
var user_file = '../input.txt';
const shapeValue = [1, 2, 3];

function winOrLose(opp, my){
    if(opp == 1 && my == 3){
        return 0;
    }else if(opp == 3 && my == 1){
        return 6;
    }else{
        return my > opp ? 6 : 0;
    }
}

function totalScore(scoreText){
    let scoreArray = [scoreText[0].charCodeAt() % 65, (scoreText[1].charCodeAt() % 65) - 23];
    let oppChoice = shapeValue[scoreArray[0]];
    let myChoice = shapeValue[scoreArray[1]];
    let completeScore = 0;

    if(myChoice == oppChoice){
        completeScore += myChoice + 3;
    }else{
        completeScore += myChoice + winOrLose(oppChoice, myChoice);
    }

    return completeScore;
}

function scoreRound(round){
    let shapes = round.split(" ");
    let finalScore = totalScore(shapes);
    return finalScore;
}

async function processStrategy(){
    var r = readline.createInterface({
        input : f.createReadStream(user_file)
    });
    for await (const line of r) {
        strategy.push(line);
    }
    let roundSum = 0;
    for(const roundScore of strategy){
        roundSum += scoreRound(roundScore);
    }
    console.log(roundSum);
}

processStrategy();
